//Pascal Bugnion, Patrick R. Nicolas, Alex Kozlov - Scala_Applied Machine Learning (2017, Packt Publishing)


/*BREEZE - a library for fast linear algebra
and manipulation of data arrays as well as many other features
necessary for scientific computing and data science

basic linear algebra operations underlying Breeze rely on the
netlib-java library, which can use system-optimized BLAS and
LAPACK libraries

*/

import breeze.linalg._
import breeze.numerics._

object breeze_tryout {
  def main(s: Array[String]): Unit = {


    // linspace is to create range of numbers
    val vec = linspace(-2.0, 2.0, 10)
    vec.foreach(println)


    // DenseVector is like list or array of numbers also called as vector
    val v = DenseVector(1.0, 2.0, 3.0)
    println(v(1))
    println(v :* 2.0) // this will throw error if the type is not the same, element wise multiplication
    println(v :+ DenseVector(2.0,3.0,4.0)) // elementwise operation of addition, will throw error if not of same length

    // more on element wise operations : (https://github.com/scalanlp/breeze/wiki/Linear-Algebra-Cheat-Sheet).

    println(v dot v :* 2.0) // dot product
    println(v :+ v :* 2.0)
    println(v + v :* 2.0) // though both do almost the same work , :+ and :* have very low importance and will be evaluated last


    // SPARSE VECTORS
    /* When dealing with arrays of
    numbers that are mostly zero, it may be more computationally efficient
    to use sparse vectors

    The SparseVector instances are very memory-efficient, but adding nonzero
    elements is slow. HashVector is more versatile, at the cost of an
    increase in memory footprint and computational time for iterating over
    non-zero elements. Unless you need to squeeze the last bits of memory
    out of your application, I recommend using HashVector

    All the three are from Vector trait

    */

  }
}

/*
-2.0
-1.5555555555555556
-1.1111111111111112
-0.6666666666666667
-0.22222222222222232
0.22222222222222232
0.6666666666666665
1.1111111111111107
1.5555555555555554
2.0
2.0
DenseVector(2.0, 4.0, 6.0)
DenseVector(3.0, 5.0, 7.0)
28.0
DenseVector(4.0, 8.0, 12.0)
DenseVector(4.0, 8.0, 12.0)

 */
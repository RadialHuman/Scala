//Pascal Bugnion, Patrick R. Nicolas, Alex Kozlov - Scala_Applied Machine Learning (2017, Packt Publishing)


/*

*/

import breeze.linalg._
import breeze.numerics._

object breeze_tryout3 {
  def main(s: Array[String]): Unit = {

    // matrix and vector creation
    println(DenseVector.ones[Int](5))
    println(DenseVector.zeros[Double](5))
    println(linspace(0,10,10))

    // matrix and vector creation using functions
    println(DenseVector.tabulate(4){i => 5.0*i})
    println(DenseMatrix.tabulate(2,3) {
      (i,j) => i*2 + j
    })

    // random value for vector and matrix
    println(DenseMatrix.rand(2,3))
    println(DenseVector.rand(10))

    // from array
    println(DenseVector(Array(1,2,3)))

    // splat -  :_ *
    var l = List(1,2,3,4,5)
    println(l)
    println(DenseVector(l :_ *)) // converts other types to denseVectors

    // indexing and slicing
    // this only creates a view and does not copy so any updatation will affect the main vector
    var x  = DenseVector.tabulate(5) {_.toDouble}
    println(x)
    println(x(-1)) // like in python and unlike scala, this works
    println(x( 1 to 3)) // including 1 2 and 3
    println(x(1 until 3)) // excludes the last element
    println(x.length-1 to 0 by -1) // reverse

    // matrix slicing
    var m  = DenseMatrix((1.0,2.0,3.0),(4.0,5.0,6.0))
    println(m(1,2))
    println(m(-1,-2))
    println(m(0 to 1, 0 to 1))
    println(m(::,1)) // all the elements along one direction
    println(m(::,0))
    println(m(1,::))
    println(m(0,::))

    // These vectors and matrices unlike other collections are mutable
    m(1,2) = 6.5
    println(m)

    m(0 to 1,1) := DenseVector(0.0,0.0)
    println(m)

    // views, when mutated, affects the main variable, to avoid that, use copy
    var y = x(0 to 2).copy
    println(y)
    println(x)


  }
}

/*

DenseVector(1, 1, 1, 1, 1)
DenseVector(0.0, 0.0, 0.0, 0.0, 0.0)
DenseVector(0.0, 1.1111111111111112, 2.2222222222222223, 3.3333333333333335, 4.444444444444445, 5.555555555555555, 6.666666666666667, 7.777777777777779, 8.88888888888889, 10.0)
DenseVector(0.0, 5.0, 10.0, 15.0)
0  1  2
2  3  4
0.9072050919533083   0.1492459113275426   0.7744786829022086
0.08635257577556565  0.05703306044540435  0.6338735534371267
DenseVector(0.6098021759620635, 0.2920929766264089, 0.08964313348627195, 0.13508949518368785, 0.9931959240173431, 0.20643160551542183, 0.23974990390447504, 0.2699078342351098, 0.49139336786465826, 0.03728143133653372)
DenseVector(1, 2, 3)
List(1, 2, 3, 4, 5)
DenseVector(1, 2, 3, 4, 5)
DenseVector(0.0, 1.0, 2.0, 3.0, 4.0)
4.0
DenseVector(1.0, 2.0, 3.0)
DenseVector(1.0, 2.0)
Range(4, 3, 2, 1, 0)
6.0
5.0
1.0  2.0
4.0  5.0
DenseVector(2.0, 5.0)
DenseVector(1.0, 4.0)
Transpose(DenseVector(4.0, 5.0, 6.0))
Transpose(DenseVector(1.0, 2.0, 3.0))
1.0  2.0  3.0
4.0  5.0  6.5
1.0  0.0  3.0
4.0  0.0  6.5
DenseVector(0.0, 1.0, 2.0)
DenseVector(0.0, 1.0, 2.0, 3.0, 4.0)



 */
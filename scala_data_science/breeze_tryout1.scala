import breeze.stats.distributions._
import breeze.linalg._
import breeze.numerics._

// https://github.com/scalanlp/breeze/wiki/Linear-Algebra-Cheat-Sheet

object breeze_1 {
  def main(s: Array[String]): Unit = {

    // to create a list of elements with start stop and jumps
    var x = linspace(1,10,5)
    x.foreach(println)
    print(x(1)) // prints the second element
    print(sigmoid(x))

    var y = DenseVector(1,2,3)  // vectors
    print(y:*2) // element-wise multiplication
    print(y:+DenseVector(3,2,1))  // vector addition




//    var x = DenseVector.zeros[Double](5)
//    print(x)



  }
}

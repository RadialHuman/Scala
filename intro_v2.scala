// source : https://www.youtube.com/watch?v=RtUkOjq38Oc
// Introduction to Scala Meetup Part 2: Expressions, Functions, and Collections

// expressions are valid code
val y ={  // expression block which returns last line of code
val x =24
x*2
}
println(y) 

val sum = {{{{2}}}}
println(sum)

// if else has expressions in it too 
// if (boolean) exp else expressions
if(y>50){
	println("more")
} else{
	println("less")
}
// or
val out1 = if(y>5){"more"} else {"less"}
println(out1) 
// or 
val out2 = if(y>5) "more" else "less"
println(out2) 

// does not have else if


//PATTERN MATCHING
/*
expression match {
	case pattern => expression
}
*/
var parts = List(1,4)
val num = parts match{
		case List(12,34) => true
		case List(1,4) => true
		case List(10,54) => false
	}
println(num)

// with conditions

var parts = List(1,4)
//println(parts.size)

val num = parts match{
		case List(12,34) => false
		case List(1,4) if parts.size>1 => true
		case List(10,54) => false
	}
println(num)

//FUNCTIONS
// def function = expression 
def sec_count:Unit ={
	val now = System.currentTimeMillis // java lib
	var secs = now/1000
	return(secs)
}

var count: Unit = sec_count
println(count)

// Exercise
def area_circle(a:Float): Double = {
	return(3.14*a*a)
}
println(area_circle(15))


// functions are dat that can be passed and stored
// therefore functions have type

def func(x:Int):Unit={
	println("the value passed is "+x)
}

var out: (Int) => Unit = func
out(23) // referring a function in a variable; takes in int and gives string output

def hell(name:String): Unit ={println(name+" to hell")} // creat a fucntion
val change: String => Unit = hell // take this value, use it to change string to unit like in the function this
change("Go") // use the value as fucntiona nd apss the parameter

//fucntion literal
//(id: type) => expression

val double = (x:Int) => x*2  // shorter way to do the same without the function
println(double(12)) 

val anotherDouble = double
println(anotherDouble(23)) // functions can be used as data and can be copied

//placeholders
val f: (Int,Int) => Int =_+_
println(f(2,3))

//COLLECTIONS
// list is immutable linked list
// eg:
var s  =List(1,2,3)
println(s.head) // first One
println(s.tail) // not head
println(s.tail.head) // first of the not head
println(s.tail.tail) // rest of the not head
println(s(0))



//higher order functions
// map fucntion
var col = List("One","two","Three")
var col_len = col.map((c:String)=> c.size) // give some logic and get it applied to the var
println(col_len) 
// can be written as
var col = List("One","two","Three")
var col_len = col.map(_.size) // give some logic and get it applied to the var
println(col_len)

// reduce -> rolls up values
var col = List("One","two","Three")
var coll = col.reduce((a,i) => a+","+i)
println(coll)

// ficnding max using reduce
var col = List("One","two","Three","four")
var coll = col.reduce((a,i) => if (a.size>i.size) a else i)
println(coll)

// no for loop so foreach
var col = List("One","two","Three","four")
col.foreach(c =>println(c.size))

// filter
var col = List("One","two","Three","four") // not iterative its declarative
println(col.filter(_.size==4)) // imperative is for loop saying what to do this say nothing about hwo to do, just do it

/* 
Arrays

> Arrays are of fixed length but ArrayBuffer can vary

*/

import scala.math._ // calls all under math package
import scala.collection.mutable.ArrayBuffer

object c3 {
  def main(s: Array[String]): Unit = {

  	var s = Array("abc","xyz")
    println(s(0))
    s(0) = "def"
    s.foreach(println) // lenght is fixed but the elements can change

    var d  =  new ArrayBuffer[Int]

    d += (4,5,6)
    println(d)
    d ++=  Array(7,8,9)
    println(d)
    d.trimEnd(5) // this is an efficient way to reduce or increase its lenght
    println(d)
    println(d.toArray.toBuffer.getClass()) // conversion of array to buffer

    d.insert(1,6) // insert 6 after the 1st position
    println(d)
    d.insert(1,6,7,8) // insert 6 after the 1st position
    println(d)
    d.remove(2) // remove the element in 2nd position
    println(d)

    for (i <- 0 to d.length-1) println(d(i)) 
    for (i <- 0 until d.length) println(d(i))  
    for (i <- d.indices) println(d(i))  
    for (i <- d) println(i) // they all do the same job

    var new_d = for(i <- d) yield 2*i // array transformation, does not affect the original array
    println(new_d)

    new_d = for(i <- d if i>=5) yield 2*i // array transformation, with guard
    println(new_d)

    // ___________________________ Inbuilt functions _____________________________________________
    
    println(new_d.sum)
    println(Array("Qwerty","Abcdef").max)
    println(Array("Qwerty","Abcdef").sorted)


  }
}

/*

abc
def
xyz
ArrayBuffer(4, 5, 6)
ArrayBuffer(4, 5, 6, 7, 8, 9)
ArrayBuffer(4)
class scala.collection.mutable.ArrayBuffer
ArrayBuffer(4, 6)
ArrayBuffer(4, 6, 7, 8, 6)
ArrayBuffer(4, 6, 8, 6)
4
6
8
6
4
6
8
6
4
6
8
6
4
6
8
6
ArrayBuffer(8, 12, 16, 12)
ArrayBuffer(12, 16, 12)
40
Qwerty
[Ljava.lang.String;@1e178745



*/
/* 
Control structure and functions

> AN expression has a value
> Statment carries out action
> Do not use return in functions
> Exceptions work using pattern matching, doesn have switch statements
> if/else has the value it returns
> If nothing is yielded, the type is Unit 
> Block is a sequence of statements in {}
> The value of block is the value of its last expression, unless the ending is an assignment, then its unit
> Shadowing rule -> The variable name cab be same but the scope depends on the block
> Methods operate on objects but functions doesn
> Anonymouse fucntions are nameless one liner
> Procedure is a function without = before {, used for side effects
> Lazy values are not executed untill evoked, even if it is wrong and not executed, it wont be pointed outin error
> They are half way between a val and a def, but are not cost free, as it invoked a method for checking
> Exceptions : ...

*/

import scala.math._ // calls all under math package

object c2 {
  def main(s: Array[String]): Unit = {

  	var x = 10
  	val s = if (x<5) 0 else 1 // is way better than if this s =0 else s =1 <- this makes s var
  	println(s) // this if has same type in the either conditions

  	val a = if(x<5) "Something" else 0 // if has mixed type in this case <- so the type would be the super type of both the types
  	println(a)

  	// ___________________________ Format printing _________________________________
  	printf("This is a number  %s and this is another %s\n", x, s)
  	// string interpolation
  	print(f"This is a number $x and this is another ${s + 10}\n")

  	// ___________________________ Loops _________________________________
  	for (i <- 1 to 10 by 2) print(i+"\t") // i<- is generator
  	for (i <- 1 to 3 ; j <- 'a' to 'c') println(i,j) //'' means char not string 
  	for (i <- 1 to 5; j <- 1 to 3 if(i!=j)) println(i,j) // if statement is called guard

  	// yieled can be used to create new vector
  	var d  = for (i <- 1 to 10 if(i%2==0)) yield i // this is comprehension 
  	println(d.toList) // other wise it will be a vector

  	// ___________________________ Functions _________________________________

  	def func(s:String, n:Int) = { // if the function is not recursive the output type function is not required
  		println(s(n))
  		s(n)
  	}
  	func("Something",4)

  	def rec (n:Int):Int = if(n<=0) 1 else n*rec(n-1) // this is factorial and is recurssive

  	// Hindley-Milner algorithm can findout the return type but is under r&d and doesn work well with oops

  	// ___________________________ Named and default arguments _________________________________

  	def somefunc (start :String = "<<< ", middle: String , end :String = " >>>") = {
  		println(start+middle+end)
  	}
  	somefunc(middle="something")
  	somefunc("[ ","something"," ]")
  	somefunc("[ ","something")
  	somefunc(end = ">",middle="something")

  	// ___________________________ Variable arguments _________________________________

  	def sum (arg : Int*)={
  		var r = 0
  		for(i <- arg) r += i
  		r
  	}
  	var f = sum(1,1,2,3,4) // this is passed as a seq, thus cant pass a range
  	println(f)
  	f = sum(1,1,2,3,4,5)
  	println(f)
  	f = sum(1 to 10: _*) // to pass a range 
  	println(f)


  	var r  = List(1,3,2,5,4,6,5,7,6)
  	println(r.tail)
  	println(r.head)
  }
}

/*

1
0
This is a number  10 and this is another 1
This is a number 10 and this is another 11
1	3	5	7	9	(1,a)
(1,b)
(1,c)
(2,a)
(2,b)
(2,c)
(3,a)
(3,b)
(3,c)
(1,2)
(1,3)
(2,1)
(2,3)
(3,1)
(3,2)
(4,1)
(4,2)
(4,3)
(5,1)
(5,2)
(5,3)
List(2, 4, 6, 8, 10)
t
<<< something >>>
[ something ]
[ something >>>
<<< something>
11
16
55
List(3, 2, 5, 4, 6, 5, 7, 6)
1


*/
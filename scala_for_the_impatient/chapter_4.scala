/* 
Maps and tuples

> Hash Tables of maps are collections of KVP
> Maps are generally hash maps but tree maps can also be selected
> It can be both mutable and immutable


*/


object c4 {
  def main(s: Array[String]): Unit = {

  	val s = Map("a"->1,"b"->2,"c"->3,"d"->4)
    println(s)
    println(s.getClass()) // this one is immutable

    val m = scala.collection.mutable.Map("a"->1,"b"->2,"c"->3,"d"->4) // this one is mutable
    println(m)
    println(m.getClass())

    println(m("a"))
    
    // to ceck if the key is there or not, since this is a commonly used one

    val e = m.getOrElse("e",5) // checks if "e" is a key else returns 5
    println(e)
    val a = m.getOrElse("a",0)
    println(a)

    println(m.get("a")) // returns Option object of class Some

    //______________________ Updating Map values __________________________________

    m("e") = 5 // adds one in the front
    println(m) 
    m += ("f" -> 6, "g" -> 7)  // multiple additions
    println(m)

    m -=("e")
    println(m)

    // immutable ones cant be altered but can me used to create a new one
    val s_new = s+ ("e" -> 5)
    println(s_new)
    println(s_new.getClass())

    // to get a mutrable from immutable
    val s_new_mut = scala.collection.mutable.Map() ++ s_new
    println(s_new_mut.getClass())


    //______________________ Iterating over maps __________________________________

    for((i,j) <- s_new_mut) println(i,j)
    println(s_new_mut.keySet)
    println(s_new_mut.values)

    //______________________ Sorted maps __________________________________

    // for sorted map using the keys
    val s_s_k = scala.collection.mutable.SortedMap("b" -> 2, "a" -> 10)
    println(s_s_k)
    // for sorted map using the keys
    val s_s_v = scala.collection.mutable.LinkedHashMap("b" -> 20, "a" -> 10)
    println(s_s_v)

    //______________________ Tuples __________________________________

    val t = (1,2.3,"4")
    println(t)
    println(t._1)
    println(t._2)
    println(t._3)
    val (number,decimal,string) = t
    println(number)

    //______________________ Zipping __________________________________
    val x = Array("<","-",">")
    val y = Array(2,10,2)
    val z = x.zip(y)

    for((i,j) <- z) println(i * j)

  }
}

/*

Map(a -> 1, b -> 2, c -> 3, d -> 4)
class scala.collection.immutable.Map$Map4
Map(b -> 2, d -> 4, a -> 1, c -> 3)
class scala.collection.mutable.HashMap
1
5
1
Some(1)
Map(e -> 5, b -> 2, d -> 4, a -> 1, c -> 3)
Map(e -> 5, b -> 2, d -> 4, g -> 7, a -> 1, c -> 3, f -> 6)
Map(b -> 2, d -> 4, g -> 7, a -> 1, c -> 3, f -> 6)
Map(e -> 5, a -> 1, b -> 2, c -> 3, d -> 4)
class scala.collection.immutable.HashMap$HashTrieMap
class scala.collection.mutable.HashMap
(e,5)
(b,2)
(d,4)
(a,1)
(c,3)
Set(e, b, d, a, c)
HashMap(5, 2, 4, 1, 3)
TreeMap(a -> 10, b -> 2)
Map(b -> 20, a -> 10)
(1,2.3,4)
1
2.3
4
1
<<
----------
>>


*/
/* 
The Basics

> Scala is not an interpreted
> Converted into bytecodes to JVM
> val is immutable
> var is mnutable, but discouraged in pure functional programming
> functions without parameters can be without ()

*/

import scala.math._ // calls all under math package

object c1 {
  def main(s: Array[String]): Unit = {
  	val x = 42
  	println("Qwerty " + x)
  	var y = ("Qwerty " + x).toLowerCase
  	println(y)
  	println(y.getClass())

  	var z : Double = 1.2 // var or val followed by the variable name and : its type
  	println(z +" is of class "+ z.getClass() )
  	println(z.toString() +" is now of class "+ z.toString().getClass() )

	var w = 1 to 10 // also written as 1.to(10) for range
	println(w)
	w.foreach(println)

	for (i <- 1 to 10 if i%2==0) println(i)

	var q = "Tyuio"
	println(y.intersect(q)) // it is case sensitive
	println(q.sorted) // as per ascii

	println(sqrt(25)+ pow(2,5)+ min(2,Pi)) // Pi is built in math -> scala.math.Pi


	// ___________________________________ Apply method _____________________________________________
	var e  = "Werty"
	println(e(3)) // translates to e.apply(3) -> def apply(n: Int):Char, starts from 0


  }
}

/*

Qwerty 42
qwerty 42
class java.lang.String
1.2 is of class double
1.2 is now of class class java.lang.String
Range 1 to 10
1
2
3
4
5
6
7
8
9
10
2
4
6
8
10
y
Tiouy
39.0
t


*/
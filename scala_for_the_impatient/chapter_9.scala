/* 
Files and ReEx

> java.util.Scanner can also be used to read from source
> Scala.io.readInt() is used to read from user
> Source.fromURL("www....")
> Scala cant read a binary file
> Scala cant write a file and uses java.io.PrintWriter. Accepts f interpolator and not printf
> Scala cant traverse through directories and uses java.nio.file.walk
> Serialization ???
> Process control ???
> Raw string can be entered via  """..."""


*/

import scala.io.Source
import scala.util.matching.Regex

object c9 {
  def main(s: Array[String]): Unit = {

    var s = Source.fromFile("chapter_9_sample.txt","UTF-8")

    val lines = s.getLines // to get one line at a time
    for (i <- lines) println(i)

    val lines_arr = s.getLines.toArray
    println(lines_arr)

    val content = s.mkString
    println(content)

    val inter = s.buffered // prints char by char
    while (inter.hasNext) {
      println(inter.next) 
    } 
  	
    s.close() // use after all the operations are done


    val d = """// The maximum amount 234 of memory(MiB) that MarGo is allowed to use
    "margo_oom": 1000,
    """

    val reg1 = """[0-9]+""".r // for raw string and to avoid double backslash
    for (i <- reg1.findAllIn(d)) println(i)

    val x = reg1.findAllIn(d).toBuffer
    println(x)

    val x1 = reg1.findFirstIn(d) // just the first match
    println(x1)

    if(x(0).matches("[0-9]+")) println("yes") // check for match
  }
}

/*

"margo": {},

    // The maximum amount of memory(MiB) that MarGo is allowed to use
    "margo_oom": 1000,

    // you may set specific environment variables here
    // e.g "env": { "PATH": "$HOME/go/bin:$PATH" }
    // in values, $PATH and ${PATH} are replaced with
    // the corresponding environment(PATH) variable, if it exists.
    "env": {},

    // if set, whenever possible `GOPATH` will be set to `GS_GOPATH`.
    // please see `Usage & Tips` `ctrl+dot,ctrl+2`
    // section `Per-project  settings & Project-based GOPATH` for details about `GS_GOPATH`
    "use_gs_gopath": false,
[Ljava.lang.String;@ba2f4ec

234
1000
ArrayBuffer(234, 1000)
Some(234)
yes


*/
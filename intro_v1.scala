// source : https://www.youtube.com/watch?v=vI808vjwsBg
// Introduction to Scala Meetup Part 1: Intro and Basics
// variable declaration is sta tically compiled
// can also be written as: var name : String = "One"; but scala is concise
var name= "One"
println(name)

// constants because most fo the scala is immutable
val name2 = "Two"
println(name2)

/* functions parameter's type and the output type
no primitive types in scala, the I in Int is class
scala code is written in type of an expression which returns something

def twice (a:Int): Int = { // braces if multiple lines
	return a * 2
}

*/

// simpler version would be 
class one {def twice (a:Int) = a*2}
val c = new one
c.twice(3)

// datatype in scala takes care of getter and setter, constructors, hashcodes, equals
// compared to JAVA which is atleast 6 liens of code, the complier understands
// Scala uses the JVM, java libraries and api but not the java language
case class xys(name : String)

// oops and functional oriented language both
// type safe, expressive, expression oriented, limber, fast

// Strings
var st = "One"
println(st)
st = st+" Two"
println(st)
println(st.size)
println(st.replace("One","Three"))
st = st+ " Two"
st.replaceAll("Two", " One")
println(st)

// regex in scala does not need backslash
// list 
// List are genrally immputable
var a = List(1,2,3): List[Int] // list of integers
println(a)
var b = List("10","20","30") // list of strings
println(b)

// appending
var x = List(1,2)
var y = x:+3
println(y)

// all the operators in scala are functions ex:

println(List(1,2,3) ++ List(3,2,1)) // ++ is a list concatenating function
println(List(1,2,3) ::: List(3,2,1)) 
println(List(1,2,3).++(List(3,2,1))) // acts like a fucntion called with parameters

// symbols are explained in scala doc

var a =List(2,4,1,5,2,6)
var b = a.sorted
println(b)
println(a:::b)

// libraries of java jdk can be accessed
// Generics are user defined types in list ex: List[List[Int]]
/* types: 
Any
Boolean
Char
Double
Int
List
Long
String
*/	



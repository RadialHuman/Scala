object si {
  def main(s: Array[String]): Unit = {

    val name = "Some One"
    val age = 42
    val outcome = name + " is " + age + " old "
    println(outcome)
    // better was would be to use as to inducate ths is a string interpolation
    // there is formatted si where instead s there will an f
    val outcome2 = s"$name is $age old"
    println(outcome2)

    val f = (1,2,3)
    val (s,d,a) = f
    println(s"Middle one is $f._2") // does not give us what we want
    println(s"Middle one is ${f._2}") // this will allow complex stuff and give the output
    println(s"Modified a is ${(a-1)*d}")


  }
}
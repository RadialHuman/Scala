// 22. Named and Default Arguments (in Scala)
//

import java.io.PrintWriter

import scala.io.StdIn._
import scala.io.Source // for reading file

object nda {
  def main(s: Array[String]): Unit ={
    stud(quix=List(1,2,3),test= List[3,2,1],assign=List[1,2,3],)
  }
  def stud(quix:List[Int],assign:List[Int],test:List[Int]):Double={
    // calling these fucntions is a problem as the parameters are of the same type and its confusing
    // named args are  to avoid confusion which is defaultly an order based
    ???
    // order can be mixed with named
    // this is helpful when there is a default argument, so order is there as well as the way names go
    // case classes uses this

  }

}

/*


 */
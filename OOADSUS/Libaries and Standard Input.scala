// 12.  Libaries and Standard Input

//scala-lang.org has all the libraries info
// std libraries are in packages
// I/O libraries
// StdIn

import scala.io.StdIn._ // for readline , these imports can be scoped too like vals and vars

object lnsi {
  def main(s: Array[String]): Unit = {
    println("Enter a number")
    val num  = readInt()
    //??? // nothing , is a perfect expression
    println("WHats your name")
    val name = readLine()
    println(num,name)
    // ??? // can be used if not sure hwta to do , but gives an missing error

  }
}

/*
Enter a number
12
WHats your name
I dont know
(12,I dont know)

 */
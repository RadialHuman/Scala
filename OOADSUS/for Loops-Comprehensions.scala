object loops {
  def main(s: Array[String]): Unit = {
      // for comprehension not exatcly for like in other languages (foreach)
    for(i <- 1 to 10 ) { // these are egenrator
      println(i)

    }
println("******************************************")
      for(i <- 1 until 10 ) { // until generator is one less than to generator
        println(i)

      }
println("******************************************")
      for(i <- 1 to 10; if(i%3 == 0 || i%5 == 0) ){  // conditionals
        println(i)

    }
println("******************************************")
    for (i <- 'a' to 'm'){  // charecter generator
      println(i)
    }
println("******************************************")
    for (i <- 'a' to 'd'; j <- 1 to 4){  // multiple generator nested
      println(i+ " " + j)
    }
    println("******************************************")
    for{ // complex loops
      i <- 1 to 4
      j <- 'a' to 'd'
      srq = i*i
      if i > 2
    }  {
      println(i + " " + j + " " + srq)
    }
    println("******************************************")
    val something = for{ // complex loops
      i <- 1 to 4
      j <- 'a' to 'd'
      srq = i*i
      if i > 2
    } yield { // is used to make a for statement which returns unit and expression
      i -> j -> srq
    }
    println(something)
  }


}

/*

1
2
3
4
5
6
7
8
9
10
******************************************
1
2
3
4
5
6
7
8
9
******************************************
3
5
6
9
10
******************************************
a
b
c
d
e
f
g
h
i
j
k
l
m
******************************************
a 1
a 2
a 3
a 4
b 1
b 2
b 3
b 4
c 1
c 2
c 3
c 4
d 1
d 2
d 3
d 4
******************************************
3 a 9
3 b 9
3 c 9
3 d 9
4 a 16
4 b 16
4 c 16
4 d 16
******************************************
Vector(((3,a),9), ((3,b),9), ((3,c),9), ((3,d),9), ((4,a),16), ((4,b),16), ((4,c),16), ((4,d),16))


 */
// 18. Strings as Sequences (in Scala)

// when functions are tossed around and passed as veriables or is returned then its called higher order functions

object sas {
  def main(s: Array[String]): Unit = {

    // string is colelction of character
    val d  ="Whats this?"
    println(d.filter(_<'l')) //  based on ascii or unicode
    println('d'.toInt) // ascii equivalent
    println('D'.toInt)
    println(d.filter(c => "aeiou".contains(c)))
    println(d.split(" ").toList)

    // strings are like collections so same treatment as the list and arrays
  }
}

/*
Wha hi?
100
68
ai
List(Whats, this?)
 */
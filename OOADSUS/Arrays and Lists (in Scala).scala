// 13.  Arrays and Lists (in Scala)

//collection libraires in scala.collection
// for storing more than just one element
import scala.io.StdIn._ // for readline , these imports can be scoped too like vals and vars

object anl {
  def main(s: Array[String]): Unit = {
    val a = Array(1,2,3,4,5) // array is mutability, not as a val but as a specific element, but not the size of array
    for (i <- a) println(i)
    a(2) = 99
    for (i <- a) println(i)
    val b = List("qwe","rty") // immutable, but length can be changed, by adding it to the front
    println(b(0))
    val s = "123"::b
    println(s)
    for (i<- s.toArray) println(i)
    println(a.toList)

  }
}

/*
1
2
3
4
5
1
2
99
4
5
qwe
List(123, qwe, rty)
123
qwe
rty
List(1, 2, 99, 4, 5)
 */
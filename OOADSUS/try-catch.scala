// 10. try-catch (in Scala)

// used for error hadelling, this is an expression
// for things can go wrong like tye and except in python
//common example is converting string to number


object tnc {
  def main(s: Array[String]): Unit = {
    val d = "123a"
    val num = try d.toInt
    catch {
      case ex: NumberFormatException => 321
      // this can be used as partial function
    }
    println(num)
    // this iwll produce numbe rformat exception

  }
}

/*
321
 */

object ooadsus{
  def main(s:Array[String]):Unit= {
    //println("this works")

    // staticsally typed language so types
    // constant variables are declared with val
    val name = "Some one" // scala understands it
    val names: String = "Some one" // but still one can provide it the type
    // if instaed of the set type something else is entere, error line occurs
    // val d:Int = "sad"

    // varying varibles are declared with var
    var x: Int = 32
    x = x+3 // this woks in var, reassigment can happen, this cant happen in val decalred variables
    // names = "xyz" //gives an error
    // String, Int, Double, Char, Boolean all start with Caps
    // Unit : doesn not represent any, created by ()
    val s = ()

    // Tuples
    val f = "qwe" -> "rty"
    val g = (1,2)
    println(f,g)

    val (a,b) = f // used to take out values
    println(a,b)
    println(f._1,f._2)
    
  }
}

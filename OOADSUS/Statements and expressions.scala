object se {
  def main(s: Array[String]): Unit = {

    // fucntional language, so whats not declaration is a expression
    // a statement is a code to command , but does not produce value
    // exp produces a value
    // literals are the simplest expressions
    // the symbols used are fucntions built in library, unlike other languages where these are builtin into the program
    println(1 + 2) // can also be written as
    println(1.+(2)) // it acts like a fucntion
    println(1 min 2) // can alos be
    println(1.min(2)) // anything that takes in one argument can be used like this
    // in precedence, any function that has letters get the leats importance

    val x = "Sed Des "
    println(x == "Sed Des ") // does a quality check
    println(x*5) // repeatition
    println(true && true, false || true)
    println(!true)


  }
}

/*
3
3
1
1
true
Sed Des Sed Des Sed Des Sed Des Sed Des
(true,true)
false 
 */
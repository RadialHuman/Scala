object inw {
  def main(s: Array[String]): Unit = {


    var i = 0 // var is required, as this is mutable, thus avoid while
    while(i<10) { // just a statement and gives back unit
      println(i * 2) // leaving at just this will end up in an infinite 0 loop
      i = i + 1 // leaving at this will end up in an infinite negative loop
    }

     // do while
      i = 0
      do{
        println(i * 2)
        i = i + 1
      }while (i<10)


     // if
    val x = 10
    if(x<9) println("Less") else println("More")

    if(x<5) println("less than 5") else {
      println("More than 5")
      println(s"the double would be ${x*2}")


    // if can be used to declare a variable
      var d = if(x<10) "ABC" else "XYZ"
      println(d)

    // weird types
      var s = if(true) "asd" else 2 // makes s an "any" type which is useless
      var a = if(true) "asd" // gives a "unit" type which also is useless
    }
  }
}

/*
0
2
4
6
8
10
12
14
16
18
0
2
4
6
8
10
12
14
16
18
More
More than 5
the double would be 20
XYZ
 */
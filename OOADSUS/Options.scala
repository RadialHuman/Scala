// 20. Otions
// immutable lists works good with recurssion

//import scala.io.StdIn._

object opt {
  def main(s: Array[String]): Unit ={
    // used during find on collections
    // find returns an option
    val d  = Array(1,2,3,4,7,3,5,7,5,3)
    println(d.find(_<3)) // gives some in option
    println(d.find(_<10)) // gives none in option
    // the type system will force you to make a choice about the otuput of find to make a real type
    // this can be done by get() but should n be done
    // pattern matching can be used to do this
    println(d.find(_<10) match{
      case Some(i)=> println("Found something")
      case None => println("Found none")
    })

    // getOrElse() can also be used..gives output as int

    println(d.find(_<3).map(_*2))
  }
}

/*
Some(1)
Some(1)
Found something
()
Some(2)

 */
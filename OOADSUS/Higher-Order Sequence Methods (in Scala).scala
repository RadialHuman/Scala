// 17. Higher-Order Sequence Methods (in Scala)

// when functions are tossed around and passed as veriables or is returned then its called higher order functions

object hof {
  def main(s: Array[String]): Unit = {

    // foreach
    val s  = List(1,2,3,4,5)
    s.foreach(println) // used for side effects

    s.map(_+5).foreach(println) // for calculation for all the elements of colelction

    s.filter(_%2==0).foreach(println) // alternatives to for loops

    println(s.count(_%2 == 0)) // retunrs only a numbe rnot a collection

    println(s.exists(_>5))

    println(s.forall(_<5))// gives back a boolean

    println(s.find(_%4 == 0)) // return type is some, thats a problem

    println(s.partition(_<3))

    println(s.reduce(_+_)) // running through the whole thing

    //println(s.fold(_+_)) // can be passed someplace to start with

    println(s.sum, s.min,s.max)

    val d  = List("qwqw","wqwqw","ds","asw")
    println(d.minBy(_.length)) // conditional minimum selection
    println(d.min)



  }
}

/*
1
2
3
4
5
6
7
8
9
10
2
4
2
false
false
Some(4)
(List(1, 2),List(3, 4, 5))
15
(15,1,5)
ds
asw

 */
// 23. Currying and Pass-by-Name (in Scala)
//


object cpn {
  def main(s: Array[String]): Unit = {

    // def add(x:Int,y:Int):Int={}
    def add(x: Int)(y: Int): Int = x + y // curried
    val plus4 = add(4)_
    println(plus4(3))
    // pass by name : => instead of evaluating the code, it takes the code and wraps it around a func
  }
}

/*
7

 */
// 21. Text Files (in Scala)
//

import java.io.PrintWriter

import scala.io.StdIn._
import scala.io.Source // for reading file

object tf {
  def main(s: Array[String]): Unit ={
     val d = Source.fromFile("E:\\etc\\test_sample.txt") // this gives a iterator, consumed as its used, has next and next till the end
    // also has collection methods like map and filter, but the iterator changes as they go through it.
    // files can be big so this can be used for a chunk
    // the o/p is a buffer source and reads character by character
    // println(d.next)
    // this gives letetr by letter, for whole lines

    val l  = d.getLines()
    // l.foreach(println) // wont display 1 as it is consumed
    val m = l.map(l=> l.split(" ").map(_.toDouble)).toArray // splittting ons pace converting str to double and to array
    m.foreach(println)

    // all files that are opened are supposed to be closed
    d.close()


    // writing them out is not in scala so using java lib
    val w = new PrintWriter("E:\\etc\\test_sample_written.txt")
    m.foreach(row => w.println(row.sum))
    w.close()
  }
}

/*

6.0
15.0
24.0

 */
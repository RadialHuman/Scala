// 15.Range

object rng {
  def main(s: Array[String]): Unit = {

    // yet another sequence type
    println(1 to 10)
    println('a' to 'z')
    println(1 until 10)
    println( 1 to 10 by 2)
    println(1.0 to 1.5 by 0.1)
    println(10 to 1 by -1)

  }
}

/*
Range(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
NumericRange(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z)
Range(1, 2, 3, 4, 5, 6, 7, 8, 9)
Range(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)

 */
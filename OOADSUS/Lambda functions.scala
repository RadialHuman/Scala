object lf {
  def main(s: Array[String]): Unit = {

      // lambda fucntions, clousers or function literals
      // "=>" rocket is used like lambda in python
      // short namelss fucntions used and thrown away as and when required
    //val sq = x => x*x // in this scala needs type which is not menationed
    val s = (x:Double) => x*x
    // can also be written as
    // val s: DOuble => DOuble = x=> x*x
    // is simialr to
    def squ (x:Double):Double = x*x

    println(squ(10))
    println(s(10.0))

    // placeholder, can be used if one elemetn appear just once
    val d :Double => Double = _*5
    println(d(2))
    // comparision fucntion
    val less: (Double,Double) => Boolean = _<_
    println(less(10,13))


  }
}

/*
100.0
100.0
10.0
true
 */
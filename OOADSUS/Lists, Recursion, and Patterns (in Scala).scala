// 19. Lists, Recursion, and Patterns (in Scala)

// immutable lists works good with recurssion

import scala.io.StdIn._

object lrp {
  def main(s: Array[String]): Unit = {
  println("Enter something till you enter quit")
  val l = buildList()
  println(l)

  println(concatString(l))

  }

  // a function to build list using user input
  def buildList(): List[String] = {
    val input  = readLine()
    if(input  == "quit") Nil // an empty list
    else input :: buildList() // appending the input in front of the list
  }

  // ordinary way of dealnig with concat
  def concatString(words:List[String]):String={
    if(words.isEmpty)""
    else words.head + concatString(words.tail)
  }

  // alternate way by patterns
  def patCon(words:List[String]):String = words match{
    case Nil => ""
    case h :: t => h + patCon(t) //pattern involving concatenate
      
  }
}

/*
1
2
3
4
5Enter something till you enter quit

6
7
8
9
0
1
2
3
4
5
6
7
8
9
0
quit
List(1, 2, 3, 4, , 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0)

q
w
wer
ert
wsed
quit
qwwerertwsed
 */
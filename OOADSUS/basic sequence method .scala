// 16. Basic Sequence Methods (in Scala)

object bsm {
  def main(s: Array[String]): Unit = {

    val d  =List(3,6,2,7,2,6,3,7,4)
    // all these are not affecting d as its immutable, all these are new collection
    println(d.drop(2)) // first two will get dropped
    println(d.dropRight(2)) // last two dropped
    println(d.head) // first element
    println(d.tail) // the rest
    println(d.last) // teh lats element
    println(d(d.length-1)) // alsos the last element
    println(d.length)
    println(d.splitAt(3)) // splits on the index
    println(d.take(3)) // opposite fo drop
    println(d.takeRight(3))
    println(d.slice(2,7)) // subset based on index
    println(d.patch(3,List(23,43),2)) // replacement of the subset (index, new substring, numebr of elements to be replaced)
    println(d.diff(List(8,2,4,6,3,6,7))) //  set of venn with uncommon of the first displayed
    println(d.distinct) // acts like sets, remoevs duplicates, first si kept
    println(d.intersect(List(1,2,3,4,5,6,7,8))) // venn intersection
    println(d.intersect(List(1,2,3,4,5,6,7,8))) // venn union, has repeatitions

    println(d.min)
    println(d.max)
    println(d.sum)
    println(d.sorted)
    println(d.product)
    println(d.mkString)
    println(d.mkString(","))
    println(d.mkString("[",",","]")) // before add and after


    println(d.zip(d.reverse)) // creates tuples of corresponding index, with which ever is shorter
    println(d.zipWithIndex) // creates tuples with its index. enumerating



  }
}

/*
List(2, 7, 2, 6, 3, 7, 4)
List(3, 6, 2, 7, 2, 6, 3)
3
List(6, 2, 7, 2, 6, 3, 7, 4)
4
4
9
(List(3, 6, 2),List(7, 2, 6, 3, 7, 4))
List(3, 6, 2)
List(3, 7, 4)
List(2, 7, 2, 6, 3)
List(3, 6, 2, 23, 43, 6, 3, 7, 4)
List(2, 3, 7)
List(3, 6, 2, 7, 4)
List(3, 6, 2, 7, 4)
List(3, 6, 2, 7, 4)
2
7
40
List(2, 2, 3, 3, 4, 6, 6, 7, 7)
254016
362726374
3,6,2,7,2,6,3,7,4
[3,6,2,7,2,6,3,7,4]
List((3,4), (6,7), (2,3), (7,6), (2,2), (6,7), (3,2), (7,6), (4,3))
List((3,0), (6,1), (2,2), (7,3), (2,4), (6,5), (3,6), (7,7), (4,8))

 */
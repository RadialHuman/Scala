object matchs {
  def main(s: Array[String]): Unit = {
    // multi way selection
    // like switch
    // uses pattern matching like regex
    // example fizz buzz fizzbuzz
      val fb = for (i <- 1 to 20){
        // if can be done but for complex cases, match has to be used
      (i%3 , i%5) match {  // _ is anything
          case(0,0) => println("fizzbuzz")
          case(0,_) => println("fizz")
          case(_,0) => println("buzz")
          case _ => println(i)  // return type is any

        }
      }

    // if guards
    val fb10 = for (i <- 1 to 20){
      // if can be done but for complex cases, match has to be used
      (i%3 , i%5) match {  // _ is anything
        case(0,0) if i > 10 => println("fizzbuzz")
        case(0,_) => println("fizz")
        case(_,0) => println("buzz")
        case _ => println(i)  // return type is any

      }
    }


  }


}

/*

1
2
fizz
4
buzz
fizz
7
8
fizz
buzz
11
fizz
13
14
fizzbuzz
16
17
fizz
19
buzz

1
2
fizz
4
buzz
fizz
7
8
fizz
buzz
11
fizz
13
14
fizzbuzz
16
17
fizz
19
buzz
1
2
fizz
4
buzz
fizz
7
8
fizz
buzz
11
fizz
13
14
fizzbuzz
16
17
fizz
19
buzz

 */
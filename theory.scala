// source: https://www.youtube.com/playlist?list=PLkz1SCf5iB4dZ2RNKCu7W9o2OtZweGY6x
/*
Functional programming: way of writting codes using only pure function and immutable values.
List of FP traits:
1. Pure functions and no side effects
2. Referential Transperency
3. First class functiona dn higher order functions
4. Anonymous functions
5. Immutability
6. Recursion and tail recursion
7. Statements
8. Strict and lazy evaluation
9. Pattern matching
10. clousers

Why FP: library design and data crunching

Details:
1. Pure functions 
	> relation between input output and relation
	> the input determines the output solely
	> no other thing in play
	> no call by value and call by reference
	> does not change the input
	> function does not do anything else apart from cimputing the output
	> does not read or write anything into the console
	> does not modify global variable or anythign outside
	> no i/o
	> no side effect
	+ encourages safe way of programming
	+ no surprise for the reader
	+ small, precise, simple and safe
	+ reusable
	+ composable or modular can be cascadded easily
	+ easy to test known input and output relation
	+ Memoizable : caching or deterministic fucntions
	+ cache the output for optimization
	+ lazy


2. Referential Transperency
	> is used to chck if a function is pure
	> used to validate purity of a fucntion
	> a func is RT when we can repalce the corresponding value without changing the code's behaviour
	> ...

3. First class function and higher order functions
 	> fucntion that can be treated as a value
 	> 1. assign a function to a variable
 	> 2. pass fucntion as a parameter to a fucntion
 	> 3. function that can be returneed by another function
 	> all fucntions are by default first class in scala
	+ higher level abstraction is the aim of this style
	+ fucntion passing makes application eaisly extendable
	+ it communicate the beahivor and less the implementation (its declarative not imperative  )
	+ less loop and more map or fucntions similar to that
	> 


4. Anonymous functions
	> must either have one or more function as argument
	> or return function as result
	> (i:Int)=>{i*2}:Int <- function literal
	> this can be assigned to a variable and can be calle dusing that variable
	> this is used if it has to be used once in an inline function
	> acts like an expression
	> easy to write and understand
	>...



5. Immutability
	> values inintialized cant/shouldn be modified
	> var can be changed
	> val cant be
	> Scala being hybrid allows both
	> but while suing it as a FP it preferes val (immutable)
	+ adopt a mathematical approach to solving
	+ allows to use the threads properly, as the values do not chnage on other threads
	+ thus making it thread safe
	+ avoids corruption of data
	+ avoids human errors that results in loss of data

6. Recursion and tail recursion
	> it is used extensively to avoid changing value of variable
	> powerful tool that calls itself
	> tail and head, tail is rest and head is first
	> all recursive algorithms works on a list
	> 1. identify the list
	> 2. implement the termination condition
	> 3. compute the head and recurs with the tail
	> next head should always be ready or else the previous one is used from the cache
	> once termination condition is met, then it roles up to perform the task
	- but this can eb a limitation
	- loop is better than recurrsion in case fo memory requirement 
	> we need to optimize the recursive
	> tail recursion is used to optimize 
	> in tail, recursive call is the last thing to do
	> exception can be used to check the stack in case of recursion

each time value has to be modified, it is stored in another val that way var can be avoided

7. Statements
	> small element that expresses some action to be carries our some action
	> ex: decalaration/initialization or println or if etc..
	> in FP, functional statement returns a value
	> this is called expression
	+ this reduces number of variables
	+ thus reaching immutability

8. Strict and lazy evaluation
	> strict (default) : evaluate expression now ex: variable assignment and function parameters
	> lazy : evaluate when first used ex: higher order fucntion
	> lazy can be avoided using caching fucntion as val
	> lazy val = l <- used to initialize lazy
	> lazy is evaluated only once
	> cahced fucntion can be to a lazy val there making the whole thing lazy 
	+ used to make big datasets operate easily
	+ list is string
	+ stream is lazy and can be used as an efficient alternative to strict list
	+ combines operations 
	+ list applies all oprations to all the lines one by one, whereas stream applies all operation to lines 1/1 there by making things faster
	+ # is used between two streams

9. Pattern matching
	> back from dead
	> not just regex
	> object matching , type checking, 
	> looks like a switch statement
	> 1. deconstruction of object
	> 2. type checking
	> 3. testing conditions and taking action


10. clousers
	> is a fucntion
	> can be pure or impure
	> free avriable is a var which is not bound to a fucntion
	> any function using a free variable is a clousure
	> free var is searched in the neaest place where the fucntion was defined
	> if the free var keeps changing, the latest var is used
	> can be pure or impure depending on the type of free variable
	> changes made inside clousre can modify the free vairbale outside
	+ this is to gain the advantage oops has over fp
	> in oops variable and method go together, but in fp its just a method
	> this saves complicated and simplifies solution
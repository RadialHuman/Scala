// http://aperiodic.net/phil/scala/s-99/


// P03 (*) Find the Kth element of a list.


object sample{
	def main(s: Array[String]): Unit = {
		def kthElement(x : List[Int], y: Int): Int = {
			(y,x) match {
				case (0, h::_) => h
				case (y, _::tail) => kthElement(tail, y-1) 
				case _ => throw new NoSuchElementException
			}
		}
		println(kthElement(List(4,2,5,2,6,1,3,4),3))
	}
}

/*

OUTPUT
------
2

*/

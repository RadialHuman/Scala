// http://aperiodic.net/phil/scala/s-99/


// P01 (*) Find the last element of a list.


object sample{
	def main(s: Array[String]): Unit = {

		// using tail recursion in reverse the list 

		def last1(x: List[Int], y: List[Int] = Nil): List[Int] ={

			x match{
				case Nil => y
				case (x :: rest) => last1(rest, x :: y)
			}

		}
		// simply reverse using reverse and read the head
		def last2(x: List[Int]): Int = x.reverse.head
		// just call the last fucntion
		def last3(x: List[Int]): Int = x.last

		println(last1(List(1,2,3,4)).head)
		println(last2(List(1,2,3,4)))
		println(last3(List(1,2,3,4)))

	}
}

/*

OUTPUT
------

4
4
4



*/

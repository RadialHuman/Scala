// http://aperiodic.net/phil/scala/s-99/


// P02 (*) Find the last but one element of a list.


object sample{
	def main(s: Array[String]): Unit = {

		// reverse and read the head of the tail
		def lastButOne1(x : List[Int]): Int = x.reverse.tail.head
		println(lastButOne1(List(4,3,2,1)))
	}
}

/*

OUTPUT
------
2

*/

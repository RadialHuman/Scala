// http://aperiodic.net/phil/scala/s-99/


// P05 (*) Reverse a list.


object sample{
	def main(s: Array[String]): Unit = {
		def rl(x:List[Int]):List[Int] = {
			def reverseList(z : List[Int], y: List[Int]): List[Int] = y match {
					case Nil => z
					case h::tail => reverseList(h::z, tail)
				}
				reverseList(Nil,x)
			} 
		println(rl(List(1,2,3,4,5,6,7)))
	}	
}

/*

OUTPUT
------
List(7, 6, 5, 4, 3, 2, 1)


*/

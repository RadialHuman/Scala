// http://aperiodic.net/phil/scala/s-99/


// P04 (*) Find the number of elements of a list.


object sample{
	def main(s: Array[String]): Unit = {
		
		// using recurssion
		def lengthFinder(x : List[Int]): Int = {
			x match {
				case Nil => 0
				case h::tail => 1 + lengthFinder(tail)
			}
		}

		println(lengthFinder(List(4,2,5,2,6,1,3,4)))
	}
}

/*

OUTPUT
------
8

*/

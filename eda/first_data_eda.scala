// data set from ssa us baby names
// has state WY's data, with gender, year,name and coutn of same name
// tryign to use learnt fucntions to get some result
// data cleaning : the count and state are merged
val source = io.Source.fromFile("E:\\Projects\\Scala\\datasets\\WY.txt").mkString
//println(source)
val source_com = source.replace("WY",",WY")
//println(source_com)
//println(source.length)
//println(source(0))
var split_source = source_com.split(",").toList
//println(split_source) // or else it would give [Ljava.lang.String;@ with simple print

println(split_source(6)) // now that its an array of elements, recurssion can be used for placing them in columns
/*
var splited = split_source.grouped(5).toList // has a problem as count and states are merged
print(splited)

*/
var gender = split_source.filter(n=>(n=="F" || n=="M"))
//println(gender)
println("gender done ",gender.length==split_source.length/5)
//println(split_source(4))

var names = split_source.filter(n=>(n matches ".*[a-z].*"))
//println(names)
println("names done ",names.length==split_source.length/5)

var years = split_source.filter(n=>(n matches ".*[0-9]"))
//println(years)
println("years done ",years.length==split_source.length/5)

var counts = split_source.map(n=>(n.trim)).filter(n=>(n.length<4 && !n.contains(".[A-Z]") && !n.matches("[A-Z]*") && !n.matches("[A-Z][a-z]") && !n.matches("[A-Z][a-z][a-z]") ))
//var counts_trim = counts.map(n=>(n.trim))
//println(counts)
println("counts done ",counts.length==split_source.length/5)

println(gender(0),names(0),years(0),counts(0))
val y = years.map(n=>n.toInt)
println(y(0))
val c = counts.map(n=>n.toInt)
println(c(0))

var min = y.head
var max = y.reverse.head
println("*********In the state of WY, the number of people who were named between %d and %d were %d " format(min,max,c.foldLeft(0){(m:Int,n:Int)=>m+n}))

var sort_n = names.sorted
sort_n.length
println("*********Out of %d total names, %d names are unique".format(sort_n.length,sort_n.toSet.size))

var only_m = gender.filter(n=>n=="M")
var only_f = gender.filter(n=>n!="M")
println("*********Of the %d names used, %d were considered male and %d, female" format(only_m.size+only_f.size,only_m.size,only_f.size))

// next up make a tuple out of 4 lists

/* Did not work
// making them flat zip, did not work yet
def flat_zip(n:(((String,String),String),String))={
	List(n(0)._1._1._1,n(0)._1._1._2,n(0)._1._2, n(0)._2)
}
var tables = table.map(n=>(flat_zip(n)))
print(tables)
// to get years

try{ var year = split_source.filter(n=>(n.toInt>1900))
println(year)
}

catch {
	case e: java.lang.NumberFormatException => println("Try again")
}
*/ 

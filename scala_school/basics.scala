// http://twitter.github.io/scala_school/

// http://twitter.github.io/scala_school/basics.html

// most of this is covered in IPPSUS
// run these part by part 
// coded in sublime text 3

// anonymous function
var d = (x:Int,y:Int) => x*y // ano. fucntion created
var e = d(12,_:Int) // passed to a variable with wildcard
println(e(20)) // wildcard passed via another variable

// Curried function
def multi(m:Int)(n:Int):Int= m*n // function with seperated arguments passed
var d =  multi(3)(4) //function passed with seperated args to a variabel
//println(d)
var e = multi(3)(_:Int) // wildcard implementation
var f  = e(4)
//println(f)
println(d==f,d,f)

// map function
var d = List(4,5,9,12,34,5,1,9,0) 
var e  = d.map(n=>n*2)
println(e)
var f = d.map(n=>n.toChar)
println(f)

// filter function
var d = List(1,2,3,4,5,6,7,8,9,10)
var e = d.filter((n:Int)=> n%2==0)
println(e)

// partition function
var d = List(1,2,3,4,5,6,7,8,9,10)
var e = d.partition((n:Int)=> n%2==0)
println(e)
println(e._1) // tuple parts can be accessed this way




//CLASSES
class something{ // define class
	def classFun(m:String,n:Int):String={ // define class's fcuntion
		n+m
	}
	val classVal = "class_value" // class variable
}
val cl = new something // make a constructor which can apss argument if the class accepst one
println(cl.classFun("qwerty",123)) // call class stuff
println(cl.classVal)

// skipping inheritance, abstract class,traits
//
//
//
//http://twitter.github.io/scala_school/basics2.html


// apply fucntion
class something{
	def apply() = "QWERTY"
}
val d = new something
println(d())

//Pattern matching
var d = 123
d match{
	case n if(n<20) => println("Less")
	case n if(n>20 && n<100) => println("medium")
	case n if(n>100) => println("High")
}

var d = 123
d match{
	case n if(n<20) => println("Less")
	case n if(n>20 && n<100) => println("medium")
	case _  => println("High")
}

// match types
def mat(n:Any):Any={
	n match{
		case a: Int if a<10 => a+1
		case a: Int => a-1 // like an else statement
		case c: Double if c<0.0 => c+0.1 
		case c: Double => c-0.1 // like an else statement
		case e: String => e+"123"
	}
}

println(mat(12))
println(mat(9))
println(mat(1.5))
println(mat(-0.5))
println(mat("qwerty"))

// exceptions
// scala.MatchError on previous function
def mat(n:Any):Any={
	n match{
		case a: Int if a<10 => a+1
		case a: Int => a-1 // like an else statement
		case c: Double if c<0.0 => c+0.1 
		//case c: Double => c-0.1 // like an else statement
		case e: String => e+"123"
	}
}
try mat(12.1)
catch {
	case e: scala.MatchError => println("Check the fucntion")
}

// tuples
var d = 1->2
println(d)

var e  = 1->2->3
println(e)

// Combinators
// map
var d = List(6,5,4,3,2,1)
var e = d.map(n=>n%3)
println(e)

def somefunc(i:Int):Int= i+5
var d = List(6,5,4,3,2,1)
var f = d.map(somefunc(_))
println(f)

// foreach
var d = List(6,5,4,3,2,1)
d.foreach(println)

// filter
var d = List(6,5,4,3,2,1) // do not do it with array, it giveds wrong output
var e = d.filter(_%2==0)
println(e)
// zip
var f = d.zip(e++Array(1,3,5))
println(f)

// partition
var d = List(6,5,4,10,9,8,7,3,2,1) 
var e = d.partition(_%2!=0)
println(e)
println(e._1)

// find
var d = List(6,5,4,10,9,8,7,3,2,1)  // only the first element
var e = d.find(_%2!=0)
println(e)

// drop 
var d = List(6,5,4,10,9,8,7,3,2,1)
var e = d.drop(3) // first 3 elemenst dropped
println(e)
var f = d.dropWhile(_%2==0) // only the first element that matches the criteria
println(f)

// fold
var d = List(6,5,4,-10,9,8,7,3,2,1)
var e = d.foldLeft(0){(m:Int,n:Int)=> m+n}
println(e)
var f = d.foldRight(0){(m:Int,n:Int)=> m+n}
println(f)
var g = d.foldLeft(0){(m:Int,n:Int)=> println(m+n);m+n}
println(g)
var h = d.foldRight(0){(m:Int,n:Int)=> println(m+n);m+n}
println(h)

// flatten
println(List(List(1,2),List(3,4)).flatten)

//flatmap
var d  = List(List(1,2),List(3,4))
var e = d.flatMap(_.map(_*2))
println(e)
